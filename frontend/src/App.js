import React,{Component} from 'react';
import './App.scss';
import openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:2000');

class App extends Component {
  constructor() {
    super();
    this.state = {
      welcomeMessage: 'Loading...',
      rooms: [],
      hitSound: new Audio('/hit_sound.mp3'),
      room: {},
      playerId: null,
      startGame: false
    }
  }

  createRoom(){
    socket.emit('createRoom')
  }

  componentDidMount(){
    socket.on('welcome', (data)=> {
      this.setState({
        playerId: socket.id,
        welcomeMessage: data
      })
    })
    socket.on('rooms', (data)=> {
      this.setState({rooms: data})
    })
    socket.on('err', (data)=> {
      console.log(data);
    })
    socket.on('roomJoined', (data)=> {
      this.setState({startGame: data.start_game, room: data.room})
    })
    socket.on('newPositions', (data)=> {
      this.setState({room: data})
    })
    socket.on("hitSound",()=>{
      const {hitSound} = this.state;
      hitSound.pause();
      hitSound.currentTime = 0;
      hitSound.play();
    })
    // socket.on("scoreSound",()=>{
    //   const {scoreSound} = this.state;
    //   scoreSound.pause();
    //   scoreSound.currentTime = 0;
    //   scoreSound.play();
    // })
    document.onkeydown = (event) => {
      const {room, playerId} = this.state;
      if(event.keyCode === 83)   //s
          socket.emit('keyPress',{inputId:'down', state:true, room_id: room.id, playerId});
      else if(event.keyCode === 87) // w
          socket.emit('keyPress',{inputId:'up', state:true, room_id: room.id, playerId}); 
    }
    document.onkeyup = (event) => {
        const {room, playerId} = this.state;
        if(event.keyCode === 83)   //s
            socket.emit('keyPress',{inputId:'down',state:false,room_id: room.id, playerId});
        else if(event.keyCode === 87) // w
            socket.emit('keyPress',{inputId:'up',state:false, room_id: room.id, playerId});
    }
  }
  joinRoom(room){
    socket.emit('joinRoom',room)
  }

  render() {
    const {welcomeMessage, rooms, startGame, playerId, room} = this.state;
      return (
        <div className="App">
          <div>{welcomeMessage}</div>
          {startGame && 
            <div>
              <h1 class="score" id="left-score">Player A: {room.playerA.score}</h1>
              <h1 class="score" id="right-score">Player B: {room.playerB.score}</h1>
            </div>
            }
            <div class="game-area-wrapper">
            {!startGame && 
            <div>
              <div>{rooms.length == 0 ? 'No rooms :(' : 'Join one of the rooms below'}</div>
                <button onClick={()=>{this.createRoom()}}>Create Room</button>
                {
                  rooms.length > 0 && rooms.map((room, index) => {
                    return (
                      <div key={index}>{room.id} (Player 1: {room.playerA.id || "Empty"} | Player 2: {room.playerB.id || 'Empty' }) 
                      {
                        playerId != room.playerA.id && playerId != room.playerB.id &&
                        <button onClick={()=>{ this.joinRoom(room.id)}}>Join Room</button>
                      }
                      </div>
                    )
                  })
                }
              </div>
            }
            {startGame &&
              <div className="game-area">
                <div class="paddle" ref="leftPaddle" style={{top: room.playerA.y}} id="left-paddle"></div>
                <div class="paddle" ref="rightPaddle" style={{top: room.playerB.y}} id="right-paddle"></div>
                <div class="ball" ref="ball" style={{top: room.ballPositionY, left: room.ballPositionX}} id="ball"></div>
              </div>
            }
          </div>
        </div>
      );
  }
}
export default App;
