var express = require('express');
var app = express();
var serv = require('http').Server(app);
let rooms = [];

serv.listen(2000, () => {
    console.log('Server is listening on localhost:2000')
});

var io = require('socket.io')(serv,{});

io.on("connection", (socket) => {
    socket.emit("welcome", "Hello Knoxon!")
    io.emit("rooms", rooms)
    var player = Player(socket.id);
    socket.on("createRoom", (data) => {
        const room_name = 'room-'+socket.id
        const room = new Room(room_name, player, new Player(null, room_name));
        rooms.push(room);
        io.emit("rooms", rooms)
        socket.join('room-'+ socket.id);
    })

    socket.on("joinRoom",(room_id) => {
        var room = rooms.find(x => x.id == room_id);
        if(socket.id !== room.playerA.id){
            socket.join(room.id)
            room.playerB = player;
            io.emit("rooms", rooms)
            room.gameStarted = true;
            io.in(room.id).emit("roomJoined",{start_game:true, room})
        }else{
            socket.emit("err", "You are already in this room!")
        }
    })

    socket.on('keyPress',function(data){
        const room = rooms.find(x => x.id == data.room_id);
        if(room){
            if(data.inputId === 'up'){
                if(room.playerA.id == data.playerId){
                    room.playerA.pressingUp = data.state;
                }else if(room.playerB.id == data.playerId){
                    room.playerB.pressingUp = data.state;
                }
            }
            else if(data.inputId === 'down'){
                if(room.playerA.id == data.playerId){
                    room.playerA.pressingDown = data.state;
                }else if(room.playerB.id == data.playerId){
                    room.playerB.pressingDown = data.state;
                }
            }
        }
    });

    socket.on("disconnect", (data) => {
        const room = rooms.find(x => x.playerA.id == socket.id) || rooms.find(x => x.playerB.id == socket.id)
        if(room){
            io.in(room.id).emit("roomJoined",{start_game:false, room:null})
            if(room.playerA.id == socket.id){
                room.playerA = new Player(null, room.id)
            }else{
                room.playerB = new Player(null, room.id)
            }
            if(room.playerA.id == null && room.playerB.id == null){
                rooms.splice(rooms.indexOf(room.id),1);
            }
            io.emit("rooms", rooms)
        }
    })
})

var Room = function(id, playerA, playerB) {
    var self = {
        id: id,
        gameStarted: false,
        playerA: playerA,
        playerB: playerB,
        ballPositionX: 250,
        oldBallPositionX: 250,
        ballPositionY: 250,
        oldBallPositionY: 250,
        ballXDir: 5,
        ballYDir: 5,
        scored: true,
        hitWall: false,
    }
    
    self.updateBallPosition = function(){
        if(self.gameStarted){
            self.oldBallPositionX = self.ballPositionX
            self.oldBallPositionY = self.ballPositionY
            var newBallX = self.ballPositionX + self.ballXDir;
            var newBallY = self.ballPositionY + self.ballYDir;
    
            if (newBallX + 10 > 490) {
                self.ballXDir = -Math.abs(self.ballXDir);
                self.hitWall = true;
            }
            if (newBallY > 490) {
                self.ballYDir = -Math.abs(self.ballYDir);
            }
            if (newBallX < 10) {
                self.ballXDir = Math.abs(self.ballXDir);
                self.hitWall = true;
            }
            if (newBallY < 10) {
                self.ballYDir = Math.abs(self.ballYDir);
            }
            self.ballPositionX += self.ballXDir
            self.ballPositionY += self.ballYDir
            if(self.ballPositionX > self.oldBallPositionX){
                if (self.ballPositionX == 20 && 
                    (self.ballPositionY + 20 < self.playerA.y || 
                    self.ballPositionY > self.playerA.y + 150)) {
                    self.playerA.score += 1
                    ;
                    self.ballPositionX = 250;
                    self.ballPositionY = 250;
                    self.playerA.y = 175;
                    self.playerB.y = 175;
                    self.ballXDir = 5;
                    self.scored = true;
                   
                }
            }
            if(self.ballPositionX < self.oldBallPositionX){
                if (self.ballPositionX + 20 == 480 && 
                    (self.ballPositionY + 20 <  self.playerB.y || 
                    self.ballPositionY > self.playerB.y + 150)) {
                    self.playerB.score += 1;
                    self.ballPositionX = 250;
                    self.ballPositionY = 250;
                    self.playerA.y = 175;
                    self.playerB.y = 175;
                    self.ballXDir = 5;
                    self.scored = true;
                }
            }
        }
    }
    return self;
}

var Player = function(id, connectedRoom) {
    var self = {
        y: 175,
        score: 0,
        id: id,
        connectedRoom: connectedRoom,
        pressingUp: false,
        pressingDown: false,
    }

    self.updatePosition = function(){
        if(self.pressingUp && self.y > 0){
            self.y-=25;
        } 
        if(self.pressingDown && self.y < 350){
            self.y+=25;
        }
    }
    return self;
}

setInterval(function() {
    for(var i in rooms){
        var room = rooms[i];
        room.playerA.updatePosition();
        room.playerB.updatePosition();
        room.updateBallPosition();
        if(room.hitWall == true && room.scored == true){
                io.in(room.id).emit('scoreSound');
        }else if(room.hitWall == true && room.scored == false){
            io.in(room.id).emit('hitSound');
        }
        room.hitWall = false;
        room.scored = false;
        io.in(room.id).emit('newPositions',room);
    }
}, 1000/25);